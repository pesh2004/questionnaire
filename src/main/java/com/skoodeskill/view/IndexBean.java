package com.skoodeskill.view;

import com.skoodeskill.model.Department;
import com.skoodeskill.model.Poll;
import com.skoodeskill.services.DepartmentService;
import com.skoodeskill.services.PollService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by PeshZ on 10/26/15.
 */
@ManagedBean(name="indexBean")
@ViewScoped
public class IndexBean implements Serializable {

    @ManagedProperty("#{department}")
    private DepartmentService departmentService;

    @ManagedProperty("#{poll}")
    private PollService pollService;

    private Poll pollEntity;
    private List<Department> departmentList;
    private int selectDepartment;

    @PostConstruct
    public void init() {
    }

    public void startForm(){
        pollEntity = new Poll();
        pollEntity.setJobDeScript1(1);
        pollEntity.setJobDeScript2(1);
        pollEntity.setJobDeScript3(1);
        pollEntity.setJobDeScript4(1);
        pollEntity.setJobDeScript5(1);
        pollEntity.setStable1(1);
        pollEntity.setStable2(1);
        pollEntity.setStable3(1);
        pollEntity.setStable4(1);
        pollEntity.setStable5(1);
        pollEntity.setStable6(1);
        pollEntity.setCoop1(1);
        pollEntity.setCoop2(1);
        pollEntity.setCoop3(1);
        pollEntity.setCoop4(1);
        pollEntity.setCoop5(1);
        pollEntity.setHead1(1);
        pollEntity.setHead2(1);
        pollEntity.setHead3(1);
        pollEntity.setHead4(1);
        pollEntity.setHead5(1);
        pollEntity.setHead6(1);
        pollEntity.setHead7(1);
        pollEntity.setHead8(1);
        pollEntity.setWelfare1(1);
        pollEntity.setWelfare2(1);
        pollEntity.setWelfare3(1);
        pollEntity.setWelfare4(1);
        pollEntity.setWelfare5(1);
        pollEntity.setWelfare6(1);
        pollEntity.setPlace1(1);
        pollEntity.setPlace2(1);
        pollEntity.setPlace3(1);
        pollEntity.setPlace4(1);
        pollEntity.setPlace5(1);
        pollEntity.setPlace6(1);
        pollEntity.setPlace7(1);
        pollEntity.setPlace8(1);
        pollEntity.setPlace9(1);
        pollEntity.setAttempt1(1);
        pollEntity.setAttempt2(1);
        pollEntity.setAttempt3(1);
        pollEntity.setAttempt4(1);
        pollEntity.setAttempt5(1);
        pollEntity.setAttempt6(1);
        pollEntity.setProud1(1);
        pollEntity.setProud2(1);
        pollEntity.setProud3(1);
        pollEntity.setProud4(1);
        pollEntity.setProud5(1);
        pollEntity.setPart1(1);
        pollEntity.setPart2(1);
        pollEntity.setPart3(1);
        pollEntity.setPart4(1);
        pollEntity.setSex("ชาย");
//        pollEntity.setAge("20 – 30 ปี");
//        pollEntity.setEducation("ต่ำกว่าปริญญาตรี");
//        pollEntity.setCurrentJobStatus("ปฏิบัติงาน");
//        pollEntity.setExperience("น้อยกว่า 3 ปี");
        departmentList = departmentService.getAllDepartment();
        selectDepartment = 0;
        System.out.println("start form");
    }

    public Poll getPollEntity() {
        return pollEntity;
    }

    public void setPollEntity(Poll pollEntity) {
        this.pollEntity = pollEntity;
    }

    public int getSelectDepartment() {
        return selectDepartment;
    }

    public void setSelectDepartment(int selectDepartment) {
        this.selectDepartment = selectDepartment;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public DepartmentService getDepartmentService() {
        return departmentService;
    }

    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    public void saveData(){
        try {
            int effectRow;
            System.out.println("insert");
            Department department = new Department();
            department.setId(selectDepartment);
            department.setDepartmentName(null);
            pollEntity.setDepartment(department);
            effectRow = pollService.insertPoll(pollEntity);
            System.out.println(effectRow);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public PollService getPollService() {
        return pollService;
    }

    public void setPollService(PollService pollService) {
        this.pollService = pollService;
    }
}
