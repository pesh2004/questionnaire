package com.skoodeskill.services;

import com.skoodeskill.mappers.PollMapper;
import com.skoodeskill.model.Poll;
import org.apache.ibatis.session.SqlSession;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created by PeshZ on 10/26/15.
 */
@ManagedBean(name = "poll")
@ApplicationScoped
public class PollService implements PollMapper {
    @Override
    public int insertPoll(Poll poll) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            PollMapper pollMapper = sqlSession.getMapper(PollMapper.class);
            int effectRow = pollMapper.insertPoll(poll);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }
}
