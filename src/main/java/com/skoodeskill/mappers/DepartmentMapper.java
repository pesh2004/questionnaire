package com.skoodeskill.mappers;

import com.skoodeskill.model.Department;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by PeshZ on 10/26/15.
 */
public interface DepartmentMapper {
    @Select("select * from department")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "departmentName", column = "department_name"),
    })
    public List<Department> getAllDepartment();
}
