package com.skoodeskill.mappers;

import com.skoodeskill.model.Poll;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

/**
 * Created by PeshZ on 10/26/15.
 */
public interface PollMapper {
    @Insert("insert into poll(sex, department, age, education, currentjobstatus, experience, job_descript1, job_descript2, job_descript3, job_descript4, job_descript5, stable1, stable2, stable3, stable4, stable5, stable6, coop1, coop2, coop3, coop4, coop5, head1, head2, head3, head4, head5, head6, head7, head8, welfare1, welfare2, welfare3, welfare4, welfare5, welfare6, place1, place2, place3, place4, place5, place6, place7, place8, place9, attempt1, attempt2, attempt3, attempt4, attempt5, attempt6, proud1, proud2, proud3, proud4, proud5, part1, part2, part3, part4, suggestion,suggestion_part2)values(#{sex}, #{department.id}, #{age}, #{education}, #{currentJobStatus}, #{experience}, #{jobDeScript1}, #{jobDeScript2}, #{jobDeScript3}, #{jobDeScript4}, #{jobDeScript5}, #{stable1}, #{stable2}, #{stable3}, #{stable4}, #{stable5}, #{stable6}, #{coop1}, #{coop2}, #{coop3}, #{coop4}, #{coop5}, #{head1}, #{head2}, #{head3}, #{head4}, #{head5}, #{head6}, #{head7}, #{head8}, #{welfare1}, #{welfare2}, #{welfare3}, #{welfare4}, #{welfare5}, #{welfare6}, #{place1}, #{place2}, #{place3}, #{place4}, #{place5}, #{place6}, #{place7}, #{place8}, #{place9}, #{attempt1}, #{attempt2}, #{attempt3}, #{attempt4}, #{attempt5}, #{attempt6}, #{proud1}, #{proud2}, #{proud3}, #{proud4}, #{proud5}, #{part1}, #{part2}, #{part3}, #{part4}, #{suggestion},#{suggestionPart2})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertPoll(Poll poll);
}
